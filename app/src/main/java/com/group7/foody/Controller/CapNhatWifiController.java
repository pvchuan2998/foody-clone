package com.group7.foody.Controller;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.group7.foody.Adapters.AdapterDanhSachWifi;
import com.group7.foody.Controller.Interfaces.ChiTietQuanAnInterface;
import com.group7.foody.Model.WifiQuanAnModel;
import com.group7.foody.R;

import java.util.ArrayList;
import java.util.List;

public class CapNhatWifiController {

    WifiQuanAnModel wifiQuanAnModel;
    Context context;
    List<WifiQuanAnModel> wifiQuanAnModelList;
    public CapNhatWifiController(Context context){

        wifiQuanAnModel = new WifiQuanAnModel();
        this.context = context;
    }

    public void HienThiDanhSachWifi(String maquanan, final RecyclerView recyclerView){

        wifiQuanAnModelList = new ArrayList<>();

        ChiTietQuanAnInterface chiTietQuanAnInterface = new ChiTietQuanAnInterface() {
            @Override
            public void HienThiDanhSachWifi(WifiQuanAnModel wifiQuanAnModel) {
                wifiQuanAnModelList.add(wifiQuanAnModel);
                AdapterDanhSachWifi adapterDanhSachWifi = new AdapterDanhSachWifi(context, R.layout.layout_wifi_chitietquanan,wifiQuanAnModelList);
                recyclerView.setAdapter(adapterDanhSachWifi);
            }
        };

        wifiQuanAnModel.GetListWifiQuanAn(maquanan,chiTietQuanAnInterface);

    }

    public void ThemWifi(Context context, WifiQuanAnModel wifiQuanAnModel, String maquanan){
        wifiQuanAnModel.themWifiQuanAn(context,wifiQuanAnModel,maquanan);
    }
}
