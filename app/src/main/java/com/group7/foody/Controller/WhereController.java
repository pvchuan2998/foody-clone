package com.group7.foody.Controller;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.group7.foody.Adapters.AdapterRecycleWhere;
import com.group7.foody.Controller.Interfaces.WhereInterface;
import com.group7.foody.Model.StoreModel;
import com.group7.foody.R;

import java.util.ArrayList;
import java.util.List;


public class WhereController {

    Context context;
    StoreModel storeModel;
    AdapterRecycleWhere adapterRecycleWhere;
    int itemdaco = 3;

    public WhereController(Context context){
        this.context = context;
        storeModel = new StoreModel();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void getStoreListController(Context context ,NestedScrollView nestedScrollView, RecyclerView recyclerWhere,
                                       final ProgressBar pb, final Location vitrihientai){
        final List<StoreModel> storeModelList = new ArrayList<>();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerWhere.setLayoutManager(layoutManager);
        adapterRecycleWhere = new AdapterRecycleWhere(storeModelList, R.layout.custom_layout_recyclerview_where, context);
        recyclerWhere.setAdapter(adapterRecycleWhere);



        final WhereInterface whereInterface = new WhereInterface() {
            @Override
            public void getStoreListModel(final StoreModel storeModel) {

                final List<Bitmap> bitmaps = new ArrayList<>();
                for(String linkhinh : storeModel.getHinhanhquanan()){
                    final StorageReference storagePic = FirebaseStorage.getInstance().getReference().child("hinhanh").child(linkhinh);

                    long ONE_MEGABYTE = 1024*1024;
                    storagePic.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                        @Override
                        public void onSuccess(byte[] bytes) {
                            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
                            bitmaps.add(bitmap);
                            storeModel.setBitmapList(bitmaps);

                            if(storeModel.getBitmapList().size() == storeModel.getHinhanhquanan().size()){
                                // Add storeModel into storeModeList, update adapterRecyclerWhere
                                storeModelList.add(storeModel);
                                adapterRecycleWhere.notifyDataSetChanged();

                                pb.setVisibility(View.GONE);
                            }

                        }
                    });
                }
//                storeModelList.add(storeModel);
//                adapterRecycleWhere.notifyDataSetChanged();
//
//                pb.setVisibility(View.GONE);
            }
        };


        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView nestedScrollView, int i, int i1, int i2, int i3) {
                if(nestedScrollView.getChildAt(nestedScrollView.getChildCount()- 1) !=null){
                    if(i1 >= (nestedScrollView.getChildAt(nestedScrollView.getChildCount()- 1)).getMeasuredHeight() - nestedScrollView.getMeasuredHeight()){
                        itemdaco += 3;
                        storeModel.getStoreList(whereInterface,vitrihientai,itemdaco,itemdaco-3);
                    }
                }
            }
        });

        //
        storeModel.getStoreList(whereInterface,vitrihientai,itemdaco,0);
    }
}
