package com.group7.foody.Controller;

import android.widget.TextView;

import com.group7.foody.Controller.Interfaces.ChiTietQuanAnInterface;
import com.group7.foody.Model.WifiQuanAnModel;

import java.util.ArrayList;
import java.util.List;

public class ChiTietQuanController {

    WifiQuanAnModel wifiQuanAnModel;
    List<WifiQuanAnModel> wifiQuanAnModelList;

    public ChiTietQuanController(){
        wifiQuanAnModel = new WifiQuanAnModel();
        wifiQuanAnModelList= new ArrayList<>();
    }

    public void HienThiDanhSachWifiQuanAn(String maquanan, final TextView txtTenWifi, final TextView txtMatKhauWifi, final TextView txtNgayDang){


        ChiTietQuanAnInterface chiTietQuanAnInterface = new ChiTietQuanAnInterface() {
            @Override
            public void HienThiDanhSachWifi(WifiQuanAnModel wifiQuanAnModel) {
                wifiQuanAnModelList.add(wifiQuanAnModel);
                txtTenWifi.setText("Wifi: " + wifiQuanAnModel.getTen());
                txtMatKhauWifi.setText("Password: " +wifiQuanAnModel.getMatkhau());
                txtNgayDang.setText("Ngày đăng: " +wifiQuanAnModel.getNgaydang());
            }
        };

        wifiQuanAnModel.GetListWifiQuanAn(maquanan,chiTietQuanAnInterface);
    }
}
