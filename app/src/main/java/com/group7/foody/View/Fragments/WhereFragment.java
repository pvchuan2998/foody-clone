package com.group7.foody.View.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.group7.foody.Controller.WhereController;
import com.group7.foody.Model.StoreModel;
import com.group7.foody.R;

public class WhereFragment extends Fragment {
    WhereController whereController;
    //RecyclerView rv;
    RecyclerView rv;
    ProgressBar pbWhere;
    SharedPreferences sharedPreferences;
    NestedScrollView nestedScrollView;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_fragment_where,container,false);

        rv = view.findViewById(R.id.rvWhere);
        pbWhere = view.findViewById(R.id.pbWhere);
        nestedScrollView = view.findViewById(R.id.nestScrollViewODau);

        // Get current location from sharedPreferences ( current location is passed from SlashActivity )
        sharedPreferences = getContext().getSharedPreferences("Coordinate",Context.MODE_PRIVATE);
        Location vitrihientai = new Location("");
        vitrihientai.setLatitude(Double.parseDouble(sharedPreferences.getString("Latitude", "0")));
        vitrihientai.setLongitude(Double.parseDouble(sharedPreferences.getString("Longitude","0")));


        whereController = new WhereController(getContext());

        //Log.d("testwhere",sharedPreferences.getString("Longitude","0") + "");
        whereController.getStoreListController(getContext(),nestedScrollView,rv,pbWhere,vitrihientai );

        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onStart() {
        super.onStart();
//        sharedPreferences = getContext().getSharedPreferences("Coordinate",Context.MODE_PRIVATE);
//        Location vitrihientai = new Location("");
//        vitrihientai.setLatitude(Double.parseDouble(sharedPreferences.getString("Latitude", "0")));
//        vitrihientai.setLongitude(Double.parseDouble(sharedPreferences.getString("Longitude","0")));
//
//
//        whereController = new WhereController(getContext());
//
//        //Log.d("testwhere",sharedPreferences.getString("Longitude","0") + "");
//        whereController.getStoreListController(getContext(),nestedScrollView,rv,pbWhere,vitrihientai );
    }
}
