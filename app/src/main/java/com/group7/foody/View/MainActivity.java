package com.group7.foody.View;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.group7.foody.Adapters.adapterViewPagerHomepage;
import com.group7.foody.R;

public class MainActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener, RadioGroup.OnCheckedChangeListener {

    ViewPager vpHomepage;
    RadioButton rbwhere, rbeat;
    RadioGroup radioGroup;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main_activity);

        rbwhere = findViewById(R.id.rbWhere);
        rbeat = findViewById(R.id.rbEat);
        radioGroup = findViewById(R.id.rg_homepage);

        vpHomepage = findViewById(R.id.vpHomepage);
        adapterViewPagerHomepage viewPagerHomepage = new adapterViewPagerHomepage(getSupportFragmentManager());
        vpHomepage.setAdapter(viewPagerHomepage);
        vpHomepage.addOnPageChangeListener(this);

        radioGroup.setOnCheckedChangeListener(this);
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {

            switch (i){
                case 0:
                    rbwhere.setChecked(true);
                    break;
                case 1:
                    rbeat.setChecked(true);
                    break;
            }

    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId){
            case R.id.rbEat:
                vpHomepage.setCurrentItem(1);
                break;
            case R.id.rbWhere:
                vpHomepage.setCurrentItem(0);
                break;
        }
    }
}
