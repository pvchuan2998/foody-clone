package com.group7.foody.View;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;


import com.group7.foody.Adapters.AdapterHienThiHinhBinhLuanDuocChon;
import com.group7.foody.Controller.BinhLuanController;
import com.group7.foody.Model.BinhLuanModel;
import com.group7.foody.R;

import java.util.ArrayList;
import java.util.List;

public class BinhLuanActivity extends AppCompatActivity implements View.OnClickListener {

    TextView txtTenQuanAn, txtDiachiQuanAn, txtDangBinhLuan;
    Toolbar toolbar;
    ImageButton btnChonHinh;
    EditText edtieude, ednoidung;
    RecyclerView recyclerChonHinhBinhLuan;
    AdapterHienThiHinhBinhLuanDuocChon adapterHienThiHinhBinhLuanDuocChon;

    String maquanan;
    SharedPreferences sharedPreferences;
    BinhLuanController binhLuanController;
    List<String> listHinhDuocChon;


    final int REQUEST_CHOOSE_IMG =1 ;
    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_binhluan);

        String tenquan = getIntent().getStringExtra("tenquan");
        String diachi = getIntent().getStringExtra("diachi");
        maquanan = getIntent().getStringExtra("maquan");

        sharedPreferences = getSharedPreferences("luudangnhap",MODE_PRIVATE);


        txtDiachiQuanAn = findViewById(R.id.txtDiaChiQuanAn);
        txtTenQuanAn = findViewById(R.id.txtTenQuanAn);

        txtDangBinhLuan = findViewById(R.id.txtDangBinhLuan);

        toolbar = findViewById(R.id.toolbar);

        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

        btnChonHinh = findViewById(R.id.btnChonHinh);

        txtTenQuanAn.setText(tenquan);
        txtDiachiQuanAn.setText(diachi);

        edtieude = findViewById(R.id.edTieuDeBinhLuan);
        ednoidung = findViewById(R.id.edNoiDungBinhLuan);

        recyclerChonHinhBinhLuan = findViewById(R.id.recyclerChonHinhBinhLuan);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL,false);
        recyclerChonHinhBinhLuan.setLayoutManager(layoutManager);

        binhLuanController = new BinhLuanController();
        listHinhDuocChon = new ArrayList<>();


        btnChonHinh.setOnClickListener(this);
        txtDangBinhLuan.setOnClickListener(this);

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.btnChonHinh:
                Intent intent = new Intent(this, ChonHinhBinhLuanActivity.class);
                startActivityForResult(intent,REQUEST_CHOOSE_IMG);
                break;
            case R.id.txtDangBinhLuan:
                BinhLuanModel binhLuanModel = new BinhLuanModel();
                String tieude = edtieude.getText().toString();
                String noidung = ednoidung.getText().toString();
                String mauser = sharedPreferences.getString("mauser","");
                binhLuanModel.setTieude(tieude);
                binhLuanModel.setNoidung(noidung);
                binhLuanModel.setChamdiem(0);
                binhLuanModel.setMauser(mauser);
                

                binhLuanController.ThemBinhLuan(maquanan,binhLuanModel,listHinhDuocChon);
                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_CHOOSE_IMG){
            if(resultCode == RESULT_OK){

                listHinhDuocChon = data.getStringArrayListExtra("ListHinhDuocChon");
                adapterHienThiHinhBinhLuanDuocChon = new AdapterHienThiHinhBinhLuanDuocChon(this,
                        R.layout.custom_layout_hienthibinhluanduocchon,listHinhDuocChon);

                recyclerChonHinhBinhLuan.setAdapter(adapterHienThiHinhBinhLuanDuocChon);
                adapterHienThiHinhBinhLuanDuocChon.notifyDataSetChanged();
            }
        }

    }
}
