package com.group7.foody.View;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.group7.foody.Adapters.AdapterComment;
import com.group7.foody.Controller.ChiTietQuanController;
import com.group7.foody.Model.StoreModel;
import com.group7.foody.Model.TienIchModel;
import com.group7.foody.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.SimpleTimeZone;

public class StoreDetail extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {
    TextView txtTenQuanAn, txtDiaChi, txtOpenHour , txtTrangThai, txtTongHinhAnh, txtTongBinhLuan, txtTongCheckIn, txtTongSave, txtTitle;
    TextView txtTrangThaiHoatDong;
    TextView txtTenWifi, txtMatKhauWifi, txtNgayDang;
    ImageView img;
    StoreModel quanAnModel;
    Toolbar toolbar;
    RecyclerView recyclerView;
    AdapterComment adapterComment;
    LinearLayout frameTienIch, khungWifi;
    ChiTietQuanController chiTietQuanController;
    Button btnCmtDetail, danduong;
    GoogleMap googleMap;
    Fragment mapFragment;
    LinearLayout khungTinhNang;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main_detail_store);

        quanAnModel = getIntent().getParcelableExtra("quanan");
        //Log.d("Kiemtra",quanAnModel.getGiodongcua() + "");

        txtTenQuanAn = findViewById(R.id.tenquanan);
        txtDiaChi = findViewById(R.id.diachiquanan);
        txtOpenHour = findViewById(R.id.openhour);
        txtTrangThai = findViewById(R.id.state);
        img = findViewById(R.id.image_detail);

        txtTongBinhLuan = findViewById(R.id.cmtCount);
        txtTongHinhAnh = findViewById(R.id.imgCount);
        txtTongCheckIn = findViewById(R.id.ckCount);
        txtTongSave = findViewById(R.id.saveCount);
        txtTitle = findViewById(R.id.toolbarTitle);


        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        recyclerView = findViewById(R.id.recyclerStoreDetail);
        frameTienIch = findViewById(R.id.frameTienIch);

        txtTenWifi = findViewById(R.id.txtTenWifi);
        txtMatKhauWifi = findViewById(R.id.txtMatKhauWifi);
        txtNgayDang = findViewById(R.id.txtNgayDang);
        khungWifi = findViewById(R.id.khungWifi);

        btnCmtDetail = findViewById(R.id.btnCmtDetail);

        khungTinhNang = findViewById(R.id.khungTinhNang);
        danduong = findViewById(R.id.btnChiDuong);
        danduong.setOnClickListener(this);

        khungTinhNang.setOnClickListener(this);

        mapFragment = (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map);
        ((SupportMapFragment) mapFragment).getMapAsync(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        chiTietQuanController = new ChiTietQuanController();

        DisplayDetail();

        btnCmtDetail.setOnClickListener(this);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;

    }

    @Override
    protected void onStart() {
        super.onStart();
//
//        Calendar calendar = Calendar.getInstance();
//        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
//        String currTime = dateFormat.format(calendar.getTime());
//        String openHour = quanAnModel.getGiomocua();
//        String closeHour = quanAnModel.getGiodongcua();
//
//
//        try {
//            Date dateHienTai = dateFormat.parse(currTime);
//            Date dateMoCua = dateFormat.parse(openHour);
//            Date dateDongCua = dateFormat.parse(closeHour);
//
//            if(dateHienTai.after(dateMoCua) && dateHienTai.before(dateDongCua)){
//                //gio mo cua
//                txtTrangThai.setText(getString(R.string.dangmocua));
//            }else{
//                //dong cua
//                txtTrangThai.setText(getString(R.string.dadongcua));
//            }
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//
//        txtTenQuanAn.setText(quanAnModel.getTenquanan());
////        Log.d("Kiemtra", quanAnModel.getBinhluanModelList().size() + " - " + quanAnModel.getBinhluanModelList().get(0).getThanhVienModel().getHoten());
//        txtDiaChi.setText(quanAnModel.getChiNhanhQuanAnModelList().get(0).getDiachi());
//        txtOpenHour.setText(quanAnModel.getGiomocua() + " - " + quanAnModel.getGiodongcua());
//        txtTongHinhAnh.setText(quanAnModel.getHinhanhquanan().size() + "");
//        txtTongBinhLuan.setText(quanAnModel.getBinhluanModelList().size() + "");
//        txtTitle.setText(quanAnModel.getTenquanan());
//
//        downloadHinhTienIch();
//
//        StorageReference storageHinhQuanAn = FirebaseStorage.getInstance().getReference().child("hinhanh").child(quanAnModel.getHinhanhquanan().get(0));
//        long ONE_MEGABYTE = 1024 * 1024;
//        storageHinhQuanAn.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
//            @Override
//            public void onSuccess(byte[] bytes) {
//                Bitmap bitmap = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
//                img.setImageBitmap(bitmap);
//            }
//        });
//
//        //Load list cmt
//        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(layoutManager);
//        adapterComment =  new AdapterComment(this,R.layout.custom_layout_cmt,quanAnModel.getBinhluanModelList());
//        recyclerView.setAdapter(adapterComment);
//        adapterComment.notifyDataSetChanged();
//
//        NestedScrollView nestedScrollView = findViewById(R.id.nestScrollViewChitiet);
//        nestedScrollView.smoothScrollTo(0,0);
//
//        chiTietQuanController.HienThiDanhSachWifiQuanAn(quanAnModel.getMaquanan());
    }

    private void DisplayDetail(){

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
        String currTime = dateFormat.format(calendar.getTime());
        String openHour = quanAnModel.getGiomocua();
        String closeHour = quanAnModel.getGiodongcua();


        try {
            Date dateHienTai = dateFormat.parse(currTime);
            Date dateMoCua = dateFormat.parse(openHour);
            Date dateDongCua = dateFormat.parse(closeHour);

            if(dateHienTai.after(dateMoCua) && dateHienTai.before(dateDongCua)){
                //gio mo cua
                txtTrangThai.setText(getString(R.string.dangmocua));
            }else{
                //dong cua
                txtTrangThai.setText(getString(R.string.dadongcua));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }


        txtTenQuanAn.setText(quanAnModel.getTenquanan());
//        Log.d("Kiemtra", quanAnModel.getBinhluanModelList().size() + " - " + quanAnModel.getBinhluanModelList().get(0).getThanhVienModel().getHoten());
        txtDiaChi.setText(quanAnModel.getChiNhanhQuanAnModelList().get(0).getDiachi());
        txtOpenHour.setText(quanAnModel.getGiomocua() + " - " + quanAnModel.getGiodongcua());
        txtTongHinhAnh.setText(quanAnModel.getHinhanhquanan().size() + "");
        txtTongBinhLuan.setText(quanAnModel.getBinhluanModelList().size() + "");
        txtTitle.setText(quanAnModel.getTenquanan());

        downloadHinhTienIch();

        StorageReference storageHinhQuanAn = FirebaseStorage.getInstance().getReference().child("hinhanh").child(quanAnModel.getHinhanhquanan().get(0));
        long ONE_MEGABYTE = 1024 * 1024;
        storageHinhQuanAn.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Bitmap bitmap = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
                img.setImageBitmap(bitmap);
            }
        });

        //Load list cmt
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapterComment =  new AdapterComment(this,R.layout.custom_layout_cmt,quanAnModel.getBinhluanModelList());
        recyclerView.setAdapter(adapterComment);
        adapterComment.notifyDataSetChanged();

        NestedScrollView nestedScrollView = findViewById(R.id.nestScrollViewChitiet);
        nestedScrollView.smoothScrollTo(0,0);

        chiTietQuanController.HienThiDanhSachWifiQuanAn(quanAnModel.getMaquanan(), txtTenWifi, txtMatKhauWifi, txtNgayDang);
        khungWifi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StoreDetail.this, CapNhatDanhSachWifiActivity.class);
                intent.putExtra("maquanan",quanAnModel.getMaquanan());
                startActivity(intent);
            }
        });
    }

    private void downloadHinhTienIch(){

        for(String matienich : quanAnModel.getTienich()){
            DatabaseReference noteTienIch = FirebaseDatabase.getInstance().getReference().child("quanlytienichs").child(matienich);
            noteTienIch.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    TienIchModel tienIchModel = dataSnapshot.getValue(TienIchModel.class);
                    Log.d("tommm", tienIchModel.getHinhtienich());
                    StorageReference storageHinhQuanAn = FirebaseStorage.getInstance().getReference().child("hinhtienich").child(tienIchModel.getHinhtienich());
                    long ONE_MEGABYTE = 1024 * 1024;
                    storageHinhQuanAn.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                        @Override
                        public void onSuccess(byte[] bytes) {
                            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
                            ImageView imageView = new ImageView(StoreDetail.this);
                            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(70,70);
                            imageView.setLayoutParams(layoutParams);
                            imageView.setPadding(10,5,5,5);

                            imageView.setImageBitmap(bitmap);
                            frameTienIch.addView(imageView);
                        }
                    });
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        }

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.btnCmtDetail:
                Intent intent = new Intent(this, BinhLuanActivity.class);
                intent.putExtra("tenquan", quanAnModel.getTenquanan());
                intent.putExtra("diachi", quanAnModel.getChiNhanhQuanAnModelList().get(0).getDiachi());
                intent.putExtra("maquan", quanAnModel.getMaquanan());
                startActivity(intent);
                break;
            case R.id.btnChiDuong:
                Intent iDanDuong = new Intent(this,DanDuongActivity.class);
                iDanDuong.putExtra("Latitude",quanAnModel.getChiNhanhQuanAnModelList().get(0).getLatitude());
                iDanDuong.putExtra("Longitude",quanAnModel.getChiNhanhQuanAnModelList().get(0).getLongitude());

                startActivity(iDanDuong);
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
       this.googleMap = googleMap;
       double latitude = quanAnModel.getChiNhanhQuanAnModelList().get(0).getLatitude();
       double longitude = quanAnModel.getChiNhanhQuanAnModelList().get(0).getLongitude();
       LatLng latLng = new LatLng(latitude,longitude);

       MarkerOptions markerOptions = new MarkerOptions();
       markerOptions.position(latLng);
       markerOptions.title(quanAnModel.getTenquanan());
       googleMap.addMarker(markerOptions);

        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng,14);
        googleMap.moveCamera(cameraUpdate);
    }
}
