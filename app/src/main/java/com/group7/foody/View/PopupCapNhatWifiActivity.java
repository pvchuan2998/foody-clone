package com.group7.foody.View;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.group7.foody.Controller.CapNhatWifiController;
import com.group7.foody.Model.WifiQuanAnModel;
import com.group7.foody.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class PopupCapNhatWifiActivity extends AppCompatActivity implements View.OnClickListener {

    EditText edTenWifi, edMatKhauWifi;
    Button btnOk;
    CapNhatWifiController capNhatWifiController;
    String maquanan;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_popup_capnhatwifi);


        maquanan = getIntent().getStringExtra("maquanan");

        edTenWifi = findViewById(R.id.edTenWifi);
        edMatKhauWifi = findViewById(R.id.edMatKhauWifi);
        btnOk = findViewById(R.id.btnDongYCatNhatWifi);

        btnOk.setOnClickListener(this);

        capNhatWifiController = new CapNhatWifiController(this);
    }

    @Override
    public void onClick(View v) {
        String tenwifi = edTenWifi.getText().toString();
        String mkwifi = edMatKhauWifi.getText().toString();

        if(tenwifi.trim().length() > 0 && mkwifi.trim().length()>0){
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
            String ngaydang = simpleDateFormat.format(calendar.getTime());

            WifiQuanAnModel wifiQuanAnModel = new WifiQuanAnModel();
            wifiQuanAnModel.setTen(tenwifi);
            wifiQuanAnModel.setMatkhau(mkwifi);
            wifiQuanAnModel.setNgaydang(ngaydang);
            capNhatWifiController.ThemWifi(this,wifiQuanAnModel,maquanan);
        }else{
            Toast.makeText(this,"Vui lòng nhập đầy đủ thông tin trên.",Toast.LENGTH_SHORT).show();
        }
    }
}
