package com.group7.foody.View;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.group7.foody.Adapters.AdapterRecyclerHinhBinhLuan;
import com.group7.foody.Model.BinhLuanModel;
import com.group7.foody.R;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChiTietHinhBinhLuan extends AppCompatActivity {

    CircleImageView circleImageView;
    TextView txtCaption, txtContent, txtScore;
    RecyclerView recyclerHinhBinhLuan;
    List<Bitmap> bitmapList;
    BinhLuanModel binhLuanModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_layout_cmt);

        bitmapList = new ArrayList<>();

        circleImageView = findViewById(R.id.circleImgUser);
        txtCaption = findViewById(R.id.txtCaption);
        txtContent = findViewById(R.id.txtContentComment);
        txtScore = findViewById(R.id.txtScore);

        recyclerHinhBinhLuan = findViewById(R.id.recyclerHinhBinhLuan);

        binhLuanModel = getIntent().getParcelableExtra("binhluanmodel");

        txtCaption.setText(binhLuanModel.getTieude());
        txtContent.setText(binhLuanModel.getNoidung());
        txtScore.setText(binhLuanModel.getChamdiem() + "");


        for(String linkhinh : binhLuanModel.getHinhanhList()){


            StorageReference storageHinhUser = FirebaseStorage.getInstance().getReference().child("hinhanh").child(linkhinh);
            long ONE_MEGABYTE = 1024 * 1024;
            storageHinhUser.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
                    bitmapList.add(bitmap);
                    if(bitmapList.size() == binhLuanModel.getHinhanhList().size()){

                        AdapterRecyclerHinhBinhLuan adapterRecyclerHinhBinhLuan = new AdapterRecyclerHinhBinhLuan(ChiTietHinhBinhLuan.this,
                                R.layout.custom_layout_hinhbinhluan,bitmapList,binhLuanModel,true);
                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(ChiTietHinhBinhLuan.this,2);
                        recyclerHinhBinhLuan.setLayoutManager(layoutManager);
                        recyclerHinhBinhLuan.setAdapter(adapterRecyclerHinhBinhLuan);
                        adapterRecyclerHinhBinhLuan.notifyDataSetChanged();
                    }
                }
            });
        }




    }


}
