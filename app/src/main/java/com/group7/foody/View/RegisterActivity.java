package com.group7.foody.View;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.group7.foody.Controller.RegisterController;
import com.group7.foody.Model.UserModel;
import com.group7.foody.R;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnRegister;
    EditText txtEmail, txtPass, txtPass2;
    ProgressDialog progressDialog;
    FirebaseAuth firebaseAuth;
    RegisterController registerController;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_register);

        progressDialog = new ProgressDialog(this);

        firebaseAuth=FirebaseAuth.getInstance();

        btnRegister=findViewById(R.id.btnRegister);
        txtEmail=findViewById(R.id.txtEmailRegister);
        txtPass=findViewById(R.id.txtPasswordRegister);
        txtPass2=findViewById(R.id.txtPasswordRegister2);


        btnRegister.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        progressDialog.setMessage(getString(R.string.loading_txt));
        //progressDialog.setIndeterminate(true);
        progressDialog.show();
        final String email=txtEmail.getText().toString();
        String pass=txtPass.getText().toString();
        String pass2=txtPass2.getText().toString();
        //String error =getString(R.string.error_info);

        if(email.trim().length() == 0){
            //error += "Email yet";
            Toast.makeText(this, "You did not enter email yet", Toast.LENGTH_SHORT).show();
        }else if(pass.trim().length() == 0){
            //error += "Fassword yet";
            Toast.makeText(this, "You did not enter password yet", Toast.LENGTH_SHORT).show();
        }else if( !pass.equals( pass2)){
            Toast.makeText(this, "You entered wrong password", Toast.LENGTH_SHORT).show();
        }else{
            firebaseAuth.createUserWithEmailAndPassword(email,pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                        progressDialog.dismiss();

                        // Add user to UserModel
                        UserModel userModel = new UserModel();
                        userModel.setHoten(email);
                        userModel.setHinhanh("user.png");
                        String userId = task.getResult().getUser().getUid();
                        registerController = new RegisterController();
                        registerController.addInfoUserController(userModel,userId);

                        Toast.makeText(RegisterActivity.this, "Register successful", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
}
