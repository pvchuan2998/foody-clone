package com.group7.foody.View;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.group7.foody.R;

public class ForgetPasswodActivity extends AppCompatActivity implements View.OnClickListener {
    Button sendEmail;
    TextView register;
    EditText email;
    FirebaseAuth firebaseAuth;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_forget_password);

        firebaseAuth = FirebaseAuth.getInstance();

        progressDialog=new ProgressDialog(this);
        email=findViewById(R.id.txtEmailFP);
        register=findViewById(R.id.txtRegisterFP);
        sendEmail=findViewById(R.id.btnEmailFP);

        register.setOnClickListener(this);
        sendEmail.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.txtRegisterFP:
                Intent iRegister = new Intent(ForgetPasswodActivity.this,RegisterActivity.class);
                startActivity(iRegister);
                break;

            case R.id.btnEmailFP:
                progressDialog.setMessage(getString(R.string.loading_txt));
                progressDialog.show();
                String iEmail= email.getText().toString();
                boolean testEmail = checkEmail(iEmail);

                if(testEmail){
                    firebaseAuth.sendPasswordResetEmail(iEmail).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                progressDialog.dismiss();
                                Toast.makeText(ForgetPasswodActivity.this,"Send successfully, check email account to get password",Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }else{
                    Toast.makeText(this,"Email not existed.",Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private boolean checkEmail(String email){
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}
