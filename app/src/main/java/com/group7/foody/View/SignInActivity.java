package com.group7.foody.View;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.group7.foody.R;

public class SignInActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener, FirebaseAuth.AuthStateListener {

    SignInButton btnGoogle;
    LoginButton btnFacebook;
    Button btnSignin;
    EditText txtEmail, txtPass;
    ProgressDialog progressDialog;
    GoogleApiClient apiClient;
    FirebaseAuth firebaseAuth;
    public static int CODE_SIGNIN_GOOGLE=3;
    public static int CHECK_SIGNIN_PROVIDER=0;
    CallbackManager callbackManager;
    TextView txtRegister, txtForgetPass;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.layout_sign_in);

        callbackManager= CallbackManager.Factory.create();

        firebaseAuth=FirebaseAuth.getInstance();
        firebaseAuth.signOut();
        btnGoogle=findViewById(R.id.btnGoogle);
        btnFacebook=findViewById(R.id.btnFacebook);
        txtRegister=findViewById(R.id.txtRegister);
        txtEmail=findViewById(R.id.txtEmailSignIn);
        txtPass=findViewById(R.id.txtPasswordSignIn);
        txtForgetPass=findViewById(R.id.txtForgetPass);
        progressDialog = new ProgressDialog(this);

        btnSignin=findViewById(R.id.btnSignin);
        btnSignin.setOnClickListener(this);

        txtRegister.setOnClickListener(this);
        txtForgetPass.setOnClickListener(this);


        sharedPreferences = getSharedPreferences("luudangnhap", MODE_PRIVATE);

        btnFacebook.setReadPermissions("email","public_profile");
        btnFacebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                CHECK_SIGNIN_PROVIDER=2;
                String tokenID=loginResult.getAccessToken().getToken();
                IdentifyFirebase(tokenID);
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });


        btnGoogle.setOnClickListener(this);

        CreateClientSignInGoogle();

    }


    private void CreateClientSignInGoogle(){
        GoogleSignInOptions googleSignInOptions=new GoogleSignInOptions.Builder()
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        apiClient= new GoogleApiClient.Builder(this)
                .enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,googleSignInOptions)
                .build();

    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        firebaseAuth.removeAuthStateListener(this);
    }


    // Create google signin form
    private void SignInGoogle(GoogleApiClient apiClient){
        CHECK_SIGNIN_PROVIDER=1;
        Intent iGoogle= Auth.GoogleSignInApi.getSignInIntent(apiClient);
        startActivityForResult(iGoogle,CODE_SIGNIN_GOOGLE);
    }


    //Get token id that signed in google to sign in firebase
    private void IdentifyFirebase(String tokenID){
        if(CHECK_SIGNIN_PROVIDER==1){
            AuthCredential authCredential= GoogleAuthProvider.getCredential(tokenID,null);
            firebaseAuth.signInWithCredential(authCredential);
        }else if(CHECK_SIGNIN_PROVIDER==2){
            AuthCredential authCredential=FacebookAuthProvider.getCredential(tokenID);
            firebaseAuth.signInWithCredential(authCredential);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==CODE_SIGNIN_GOOGLE){
            if(resultCode==RESULT_OK){
                GoogleSignInResult signInResult=Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                GoogleSignInAccount signInAccount=signInResult.getSignInAccount();
                String tokenID=signInAccount.getIdToken();
                IdentifyFirebase(tokenID);
            }
        }else{
            callbackManager.onActivityResult(requestCode,resultCode,data);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onClick(View v) {
        int id=v.getId();
        switch (id){
            case R.id.btnGoogle:
                SignInGoogle(apiClient);
                break;
            case R.id.txtRegister:
                Intent iResister=new Intent(SignInActivity.this,RegisterActivity.class);
                startActivity(iResister);
                break;
            case R.id.txtForgetPass:
                Intent iFP = new Intent(SignInActivity.this,ForgetPasswodActivity.class);
                startActivity(iFP);
                break;
            case R.id.btnSignin:
                signIn();
                break;
        }
    }

    private void signIn(){
        progressDialog.setMessage(getString(R.string.loading_txt));
        progressDialog.show();
        String email = txtEmail.getText().toString();
        String pass = txtPass.getText().toString();
        firebaseAuth.signInWithEmailAndPassword(email,pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(!task.isSuccessful()){
                    Toast.makeText(SignInActivity.this, "Wrong Email or Password, please check again", Toast.LENGTH_SHORT).show();
                }else{
                    progressDialog.dismiss();
                }
            }
        });
    }


    // Check if user sign in or sign out successfully
    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        FirebaseUser user=firebaseAuth.getCurrentUser();
        if(user!=null){

            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("mauser",user.getUid());
            editor.commit();

            Intent iMainPage= new Intent(this,MainActivity.class);
            startActivity(iMainPage);
        }
    }
}
