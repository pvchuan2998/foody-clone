package com.group7.foody.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.group7.foody.Model.BinhLuanModel;
import com.group7.foody.R;
import com.group7.foody.View.ChiTietHinhBinhLuan;

import java.util.List;


public class AdapterRecyclerHinhBinhLuan extends RecyclerView.Adapter<AdapterRecyclerHinhBinhLuan.ViewHolder> {

    Context context;
    int layout;
    List<Bitmap> listHinh;
    BinhLuanModel binhLuanModel;
    boolean isDetailCmt;

    public AdapterRecyclerHinhBinhLuan(Context context, int layout, List<Bitmap> listHinh, BinhLuanModel binhLuanModel, boolean isDetailCmt){

        this.context = context;
        this.layout = layout;
        this.listHinh = listHinh;
        this.binhLuanModel =binhLuanModel;
        this.isDetailCmt = isDetailCmt;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imgCmt;
        TextView txtSoHinhBinhLuan;
        FrameLayout frame;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imgCmt = itemView.findViewById(R.id.imgBinhLuan);
            txtSoHinhBinhLuan = itemView.findViewById(R.id.txtSoHinhBinhLuan);
            frame = itemView.findViewById(R.id.frame);
        }
    }

    @NonNull
    @Override
    public AdapterRecyclerHinhBinhLuan.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(layout,viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view);


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterRecyclerHinhBinhLuan.ViewHolder viewHolder, final int i) {

        viewHolder.imgCmt.setImageBitmap(listHinh.get(i));

        if(!isDetailCmt){
            if(i == 3){
                int soHinhConLai=listHinh.size() - 4;
                if(soHinhConLai > 0){
                    viewHolder.frame.setVisibility(View.VISIBLE);
                    viewHolder.txtSoHinhBinhLuan.setText("+" + soHinhConLai);
                    viewHolder.imgCmt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent cmtDetail = new Intent(context,ChiTietHinhBinhLuan.class);
                            cmtDetail.putExtra("binhluanmodel",binhLuanModel);
                            context.startActivity(cmtDetail);
                        }
                    });
                }

            }

        }

    }

    @Override
    public int getItemCount() {

        if(!isDetailCmt){
            if(listHinh.size()<4){
                return listHinh.size();
            }else{
                return 4;
            }

        }else{
            return listHinh.size();
        }

    }


}
