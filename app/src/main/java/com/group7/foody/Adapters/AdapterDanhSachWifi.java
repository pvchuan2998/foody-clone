package com.group7.foody.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.group7.foody.Model.WifiQuanAnModel;
import com.group7.foody.R;

import java.util.List;

public class AdapterDanhSachWifi extends RecyclerView.Adapter<AdapterDanhSachWifi.ViewHolder> {

    Context context;
    int resource;
    List<WifiQuanAnModel> wifiQuanAnModelList;

    public AdapterDanhSachWifi(Context context, int resource, List<WifiQuanAnModel> wifiQuanAnModelList){
        this.context = context;
        this.resource= resource;
        this.wifiQuanAnModelList = wifiQuanAnModelList;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtTenWifi, txtMatkhauWifi, txtNgayDang;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txtTenWifi = itemView.findViewById(R.id.txtTenWifi);
            txtMatkhauWifi = itemView.findViewById(R.id.txtMatKhauWifi);
            txtNgayDang = itemView.findViewById(R.id.txtNgayDang);
        }
    }

    @NonNull
    @Override
    public AdapterDanhSachWifi.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(resource,viewGroup,false);
        ViewHolder viewHolder = new ViewHolder(view);


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterDanhSachWifi.ViewHolder viewHolder, int i) {
        WifiQuanAnModel wifiQuanAnModel = wifiQuanAnModelList.get(i);

        viewHolder.txtTenWifi.setText(wifiQuanAnModel.getTen());
        viewHolder.txtMatkhauWifi.setText(wifiQuanAnModel.getMatkhau());
        viewHolder.txtNgayDang.setText(wifiQuanAnModel.getNgaydang());
    }

    @Override
    public int getItemCount() {
        return wifiQuanAnModelList.size();
    }


}
