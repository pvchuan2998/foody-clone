package com.group7.foody.Adapters;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.group7.foody.R;

import java.util.List;

public class AdapterHienThiHinhBinhLuanDuocChon extends RecyclerView.Adapter<AdapterHienThiHinhBinhLuanDuocChon.ViewHolder> {


    Context context;
    int resource;
    List<String> list;
    public AdapterHienThiHinhBinhLuanDuocChon(Context context, int resource, List<String> list){
        this.context = context;
        this.resource = resource;
        this.list = list;
    }


    @NonNull
    @Override
    public AdapterHienThiHinhBinhLuanDuocChon.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(resource,viewGroup,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterHienThiHinhBinhLuanDuocChon.ViewHolder viewHolder, int i) {
        Uri uri = Uri.parse(list.get(i));
        viewHolder.imageView.setImageURI(uri);

        viewHolder.imgXoa.setTag(i);
        viewHolder.imgXoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = (int) v.getTag();
                list.remove(pos);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView, imgXoa;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imgChonHinhBinhLuan);
            imgXoa = itemView.findViewById(R.id.imgXoa);
        }
    }
}
