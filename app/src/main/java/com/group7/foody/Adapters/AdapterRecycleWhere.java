package com.group7.foody.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.group7.foody.Model.BinhLuanModel;
import com.group7.foody.Model.ChiNhanhQuanAnModel;
import com.group7.foody.Model.StoreModel;
import com.group7.foody.R;
import com.group7.foody.View.StoreDetail;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterRecycleWhere extends RecyclerView.Adapter<AdapterRecycleWhere.ViewHolder> {

    List<StoreModel> storeModelList;
    int src;
    Context context;

    public AdapterRecycleWhere(List<StoreModel> storeModelList, int src, Context context){
        this.storeModelList= storeModelList;
        this.src=src;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtStoreNameWhere, txtComment, txtComment2, txtCaptionCom, txtCaptionCom2, txtScore1, txtScore2;
        TextView txtTotalComment, txtTotalImg, txtAvgScore;
        Button btnOrderWhere;
        ImageView imgWhere;
        CircleImageView imgUser, imgUser2;
        LinearLayout container1, container2;
        TextView txtDistance, txtAddress;
        CardView cardView;


        public ViewHolder(View itemView) {
            super(itemView);
            txtStoreNameWhere= itemView.findViewById(R.id.txtStoreName);

            btnOrderWhere = itemView.findViewById(R.id.btnOrderWhere);

            imgWhere= itemView.findViewById(R.id.imgWhere);

            txtCaptionCom = itemView.findViewById(R.id.txtCaption);
            txtCaptionCom2 = itemView.findViewById(R.id.txtCaption2);
            txtComment = itemView.findViewById(R.id.txtContentComment);
            txtComment2 = itemView.findViewById(R.id.txtContentComment2);
            imgUser = itemView.findViewById(R.id.circleImgUser);
            imgUser2 = itemView.findViewById(R.id.circleImgUser2);

            container1 = itemView.findViewById(R.id.container1);
            container2 = itemView.findViewById(R.id.container2);

            txtScore1 = itemView.findViewById(R.id.txtScore1);
            txtScore2 = itemView.findViewById(R.id.txtScore2);

            txtTotalComment = itemView.findViewById(R.id.txtTotalCommment);
            txtTotalImg = itemView.findViewById(R.id.txtTotalImg);
            txtAvgScore = itemView.findViewById(R.id.txtAvgScore);

            txtDistance = itemView.findViewById(R.id.txtDistance);
            txtAddress = itemView.findViewById(R.id.txtAddress);

            cardView = itemView.findViewById(R.id.cardViewOdau);

        }
    }

    //@NonNull
    @Override
    public AdapterRecycleWhere.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(src,viewGroup,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final AdapterRecycleWhere.ViewHolder viewHolder, int position) {
        final StoreModel storeModel = storeModelList.get(position);
        viewHolder.txtStoreNameWhere.setText(storeModel.getTenquanan());

        if(storeModel.isGiaohang()){
            viewHolder.btnOrderWhere.setVisibility(View.VISIBLE);
        }

        if(storeModel.getHinhanhquanan().size()>0){
//            StorageReference storagePic = FirebaseStorage.getInstance().getReference().child("hinhanh")
//                    .child(storeModel.getHinhanhquanan().get(0));
//            long ONE_MEGABYTE = 1024*1024;
//            storagePic.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
//                @Override
//                public void onSuccess(byte[] bytes) {
//                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
//                    viewHolder.imgWhere.setImageBitmap(bitmap);
//                }
//            });
            viewHolder.imgWhere.setImageBitmap(storeModel.getBitmapList().get(0));
        }

        if(storeModel.getBinhluanModelList().size() > 0){
            BinhLuanModel binhLuanModel = storeModel.getBinhluanModelList().get(0);
            viewHolder.txtCaptionCom.setText(binhLuanModel.getTieude());
            viewHolder.txtComment.setText(binhLuanModel.getNoidung());
            viewHolder.txtScore1.setText(binhLuanModel.getChamdiem() + "");

            if(storeModel.getBinhluanModelList().size() > 2){
                BinhLuanModel binhLuanModel2 = storeModel.getBinhluanModelList().get(1);
                viewHolder.txtCaptionCom2.setText(binhLuanModel2.getTieude());
                viewHolder.txtComment2.setText(binhLuanModel2.getNoidung());
                viewHolder.txtScore2.setText(binhLuanModel.getChamdiem() + "");
            }
            viewHolder.txtTotalComment.setText(storeModel.getBinhluanModelList().size() + "");

            // Tong hinh binh luan
            int totalComment = 0;
            double totalScore= 0;
            for(BinhLuanModel binhLuan : storeModel.getBinhluanModelList()){
                totalComment += binhLuan.getHinhanhList().size();
                totalScore += binhLuan.getChamdiem();

            }

            double avg = totalScore / storeModel.getBinhluanModelList().size();
            viewHolder.txtAvgScore.setText(String.format("%.2f",avg));

            if(totalComment >0){
                viewHolder.txtTotalImg.setText(totalComment + "");
            }


//            StorageReference storageHinhUser = FirebaseStorage.getInstance().getReference().child("hinhthanhvien").child(binhLuanModel.getThanhVienModel().getHinhanh());
//            long ONE_MEGABYTE = 1024 * 1024;
//            storageHinhUser.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
//                @Override
//                public void onSuccess(byte[] bytes) {
//                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
//                    viewHolder.imgUser.setImageBitmap(bitmap);
//                }
//            });

        }else{
            viewHolder.container1.setVisibility(View.GONE);
            viewHolder.container2.setVisibility(View.GONE);
            //viewHolder.txtTotalComment.setText("0");

        }


        //Lấy chi nhánh quán ăn và hiển thị địa chỉ và km
        if(storeModel.getChiNhanhQuanAnModelList().size() > 0){
            ChiNhanhQuanAnModel chiNhanhQuanAnModelTam = storeModel.getChiNhanhQuanAnModelList().get(0);
            for (ChiNhanhQuanAnModel chiNhanhQuanAnModel : storeModel.getChiNhanhQuanAnModelList()){
                if(chiNhanhQuanAnModelTam.getKhoangcach() > chiNhanhQuanAnModel.getKhoangcach()){
                    chiNhanhQuanAnModelTam = chiNhanhQuanAnModel;
                }
            }
            viewHolder.txtAddress.setText(chiNhanhQuanAnModelTam.getDiachi());
            viewHolder.txtDistance.setText(String.format("%.1f",chiNhanhQuanAnModelTam.getKhoangcach()) + " km");

        }

        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,StoreDetail.class);
                intent.putExtra("quanan",storeModel);
                context.startActivity(intent);
            }
        });


    }



    @Override
    public int getItemCount() {
        return storeModelList.size();
    }


}

