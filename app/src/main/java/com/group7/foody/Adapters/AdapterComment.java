package com.group7.foody.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.group7.foody.Model.BinhLuanModel;
import com.group7.foody.R;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterComment extends RecyclerView.Adapter<AdapterComment.ViewHolder> {

    Context context;
    int layout;
    List<BinhLuanModel> binhLuanModelList;


    public AdapterComment(Context context, int layout, List<BinhLuanModel> binhLuanModelList){
        this.context = context;
        this.layout = layout;
        this.binhLuanModelList = binhLuanModelList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView circleImageView;
        TextView txtCaption, txtContent, txtScore;
        RecyclerView recyclerHinhBinhLuan;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            circleImageView = itemView.findViewById(R.id.circleImgUser);
            txtCaption = itemView.findViewById(R.id.txtCaption);
            txtContent = itemView.findViewById(R.id.txtContentComment);
            txtScore = itemView.findViewById(R.id.txtScore);

            recyclerHinhBinhLuan = itemView.findViewById(R.id.recyclerHinhBinhLuan);

        }
    }

    @NonNull
    @Override
    public AdapterComment.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(layout,viewGroup,false);
        //ViewHolder
        ViewHolder  viewHolder = new ViewHolder(view);
        return viewHolder;

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final AdapterComment.ViewHolder viewHolder, int i) {
        final BinhLuanModel binhLuanModel = binhLuanModelList.get(i);

        viewHolder.txtCaption.setText(binhLuanModel.getTieude());
        viewHolder.txtContent.setText(binhLuanModel.getNoidung());
        viewHolder.txtScore.setText(binhLuanModel.getChamdiem() + "");
        final List<Bitmap> bitmapList =new ArrayList<>();

        for(String linkhinh : binhLuanModel.getHinhanhList()){


            StorageReference storageHinhUser = FirebaseStorage.getInstance().getReference().child("hinhanh").child(linkhinh);
            long ONE_MEGABYTE = 1024 * 1024;
            storageHinhUser.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes,0,bytes.length);

                    bitmapList.add(bitmap);
                    if(bitmapList.size() == binhLuanModel.getHinhanhList().size()){

                        AdapterRecyclerHinhBinhLuan adapterRecyclerHinhBinhLuan = new AdapterRecyclerHinhBinhLuan(context,
                                R.layout.custom_layout_hinhbinhluan,bitmapList,binhLuanModel,false);
                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context,2);
                        viewHolder.recyclerHinhBinhLuan.setLayoutManager(layoutManager);
                        viewHolder.recyclerHinhBinhLuan.setAdapter(adapterRecyclerHinhBinhLuan);
                        adapterRecyclerHinhBinhLuan.notifyDataSetChanged();
                    }
                }
            });
        }



    }

    @Override
    public int getItemCount() {
        int cmtCount=binhLuanModelList.size();
        if(cmtCount > 5){
            return 5;
        }else{
            return binhLuanModelList.size();
        }

    }

//    private void setHinhAnhBinhLuan(final CircleImageView circleImageView, String linkhinh){
//        StorageReference storageHinhUser = FirebaseStorage.getInstance().getReference().child("thanhvien").child(linkhinh);
//        long ONE_MEGABYTE = 1024 * 1024;
//        storageHinhUser.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
//            @Override
//            public void onSuccess(byte[] bytes) {
//                Bitmap bitmap = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
//                circleImageView.setImageBitmap(bitmap);
//            }
//        });
//    }


}
