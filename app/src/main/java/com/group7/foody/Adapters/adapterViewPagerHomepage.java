package com.group7.foody.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.group7.foody.View.Fragments.EatWhatFragment;
import com.group7.foody.View.Fragments.WhereFragment;

public class adapterViewPagerHomepage extends FragmentStatePagerAdapter {

    EatWhatFragment eatWhatFragment;
    WhereFragment whereFragment;

    public adapterViewPagerHomepage(FragmentManager fm) {
        super(fm);

        eatWhatFragment = new EatWhatFragment();

        whereFragment = new WhereFragment();
    }

    @Override
    public Fragment getItem(int i) {
        switch (i){
            case 0 :
                return whereFragment;


            case 1 :
                return eatWhatFragment;
                default:  return null;

        }

    }

    @Override
    public int getCount() {
        return 2;
    }
}
