package com.group7.foody.Model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.group7.foody.View.BinhLuanActivity;

import java.io.File;
import java.util.List;

public class BinhLuanModel implements Parcelable {

    double chamdiem;
    //long luothich;
    private String mauser;
    String noidung;
    String tieude;
    UserModel thanhVienModel;

    protected BinhLuanModel(Parcel in) {
        chamdiem = in.readDouble();
        mauser = in.readString();
        noidung = in.readString();
        tieude = in.readString();
        mabinhluan = in.readString();
        hinhanhList = in.createStringArrayList();
        thanhVienModel = in.readParcelable(UserModel.class.getClassLoader());

    }

    public static final Creator<BinhLuanModel> CREATOR = new Creator<BinhLuanModel>() {
        @Override
        public BinhLuanModel createFromParcel(Parcel in) {
            return new BinhLuanModel(in);
        }

        @Override
        public BinhLuanModel[] newArray(int size) {
            return new BinhLuanModel[size];
        }
    };

    public String getMabinhluan() {
        return mabinhluan;
    }

    public void setMabinhluan(String mabinhluan) {
        this.mabinhluan = mabinhluan;
    }

    String mabinhluan;

    public List<String> getHinhanhList() {
        return hinhanhList;
    }

    public void setHinhanhList(List<String> hinhanhList) {
        this.hinhanhList = hinhanhList;
    }

    List<String> hinhanhList;

    public BinhLuanModel(){


    }

    public double getChamdiem() {
        return chamdiem;
    }

    public void setChamdiem(double chamdiem) {
        this.chamdiem = chamdiem;
    }

//    public long getLuothich() {
//        return luothich;
//    }
//
//    public void setLuothich(long luothich) {
//        this.luothich = luothich;
//    }

    public String getMauser() {
        return mauser;
    }

    public void setMauser(String mauser) {
        this.mauser = mauser;
    }

    public String getNoidung() {
        return noidung;
    }

    public void setNoidung(String noidung) {
        this.noidung = noidung;
    }

    public String getTieude() {
        return tieude;
    }

    public void setTieude(String tieude) {
        this.tieude = tieude;
    }

    public UserModel getThanhVienModel() {
        return thanhVienModel;
    }

    public void setThanhVienModel(UserModel thanhVienModel) {
        this.thanhVienModel = thanhVienModel;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(chamdiem);
        dest.writeString(mauser);
        dest.writeString(noidung);
        dest.writeString(tieude);
        dest.writeString(mabinhluan);
        dest.writeStringList(hinhanhList);
        dest.writeParcelable(thanhVienModel,flags);
    }

    public void ThemBinhLuan(String maQuanAn, BinhLuanModel binhLuanModel, final List<String> listHinh){
        DatabaseReference noteBinhLuan = FirebaseDatabase.getInstance().getReference().child("binhluans");
        String key = noteBinhLuan.child(maQuanAn).push().getKey();
        noteBinhLuan.child(maQuanAn).child(key).setValue(binhLuanModel).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){

                    if(listHinh.size() > 0){
                        for(String valueHinh : listHinh){
                            Uri uri = Uri.fromFile(new File(valueHinh));
                            StorageReference storageReference = FirebaseStorage.getInstance()
                                    .getReference().child("hinhanh/" + uri.getLastPathSegment());
                            storageReference.putFile(uri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                                }
                            });
                        }
                    }


                }
            }
        });

        if(listHinh.size() > 0){
            for(String valueHinh : listHinh){
                Uri uri = Uri.fromFile(new File(valueHinh));
                FirebaseDatabase.getInstance().getReference()
                        .child("hinhanhbinhluans").child(key).push().setValue(uri.getLastPathSegment());
            }
        }


    }
}
