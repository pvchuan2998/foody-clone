package com.group7.foody.Model;

import android.graphics.Bitmap;
import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.group7.foody.Controller.Interfaces.WhereInterface;
import com.group7.foody.Controller.WhereController;

import java.util.ArrayList;
import java.util.List;

public class StoreModel implements Parcelable {

    boolean giaohang;
    String giodongcua,giomocua,tenquanan,videogioithieu,maquanan;
    long giatoida;
    long giatoithieu;
    long luotthich;
    List<String> tienich;
    List<String> hinhanhquanan;
    List<BinhLuanModel> binhluanModelList;
    private DatabaseReference noteRoot;
    List<Bitmap> bitmapList;
    List<ChiNhanhQuanAnModel> chiNhanhQuanAnModelList;

    protected StoreModel(Parcel in) {
        giaohang = in.readByte() != 0;
        giodongcua = in.readString();
        giomocua = in.readString();
        tenquanan = in.readString();
        videogioithieu = in.readString();
        maquanan = in.readString();
        giatoida = in.readLong();
        giatoithieu = in.readLong();
        luotthich = in.readLong();
        tienich = in.createStringArrayList();
        hinhanhquanan = in.createStringArrayList();
        //bitmapList = in.createTypedArrayList(Bitmap.CREATOR);

        chiNhanhQuanAnModelList = new ArrayList<ChiNhanhQuanAnModel>();
        in.readTypedList(chiNhanhQuanAnModelList,ChiNhanhQuanAnModel.CREATOR);

        binhluanModelList = new ArrayList<BinhLuanModel>();
        in.readTypedList(binhluanModelList,BinhLuanModel.CREATOR);
    }

    public static final Creator<StoreModel> CREATOR = new Creator<StoreModel>() {
        @Override
        public StoreModel createFromParcel(Parcel in) {
            return new StoreModel(in);
        }

        @Override
        public StoreModel[] newArray(int size) {
            return new StoreModel[size];
        }
    };

    public List<Bitmap> getBitmapList() {
        return bitmapList;
    }

    public void setBitmapList(List<Bitmap> bitmapList) {
        this.bitmapList = bitmapList;
    }



    public List<ChiNhanhQuanAnModel> getChiNhanhQuanAnModelList() {
        return chiNhanhQuanAnModelList;
    }

    public void setChiNhanhQuanAnModelList(List<ChiNhanhQuanAnModel> chiNhanhQuanAnModelList) {
        this.chiNhanhQuanAnModelList = chiNhanhQuanAnModelList;
    }



    public List<BinhLuanModel> getBinhluanModelList() {
        return binhluanModelList;
    }

    public void setBinhluanModelList(List<BinhLuanModel> binhluanModelList) {
        this.binhluanModelList = binhluanModelList;
    }






    public List<String> getHinhanhquanan() {
        return hinhanhquanan;
    }

    public void setHinhanhquanan(List<String> hinhanhquanan) {
        this.hinhanhquanan = hinhanhquanan;
    }



    public boolean isGiaohang() {
        return giaohang;
    }

    public void setGiaohang(boolean giaohang) {
        this.giaohang = giaohang;
    }

    public String getGiodongcua() {
        return giodongcua;
    }

    public void setGiodongcua(String giodongcua) {
        this.giodongcua = giodongcua;
    }

    public String getGiomocua() {
        return giomocua;
    }

    public void setGiomocua(String giomocua) {
        this.giomocua = giomocua;
    }

    public String getTenquanan() {
        return tenquanan;
    }

    public void setTenquanan(String tenquanan) {
        this.tenquanan = tenquanan;
    }

    public String getVideogioithieu() {
        return videogioithieu;
    }

    public void setVideogioithieu(String videogioithieu) {
        this.videogioithieu = videogioithieu;
    }

    public String getMaquanan() {
        return maquanan;
    }

    public void setMaquanan(String maquanan) {
        this.maquanan = maquanan;
    }

    public long getGiatoida() {
        return giatoida;
    }

    public void setGiatoida(long giatoida) {
        this.giatoida = giatoida;
    }

    public long getGiatoithieu() {
        return giatoithieu;
    }

    public void setGiatoithieu(long giatoithieu) {
        this.giatoithieu = giatoithieu;
    }

    public long getLuotthich() {
        return luotthich;
    }

    public void setLuotthich(long luotthich) {
        this.luotthich = luotthich;
    }

    public List<String> getTienich() {
        return tienich;
    }

    public void setTienich(List<String> tienich) {
        this.tienich = tienich;
    }

    public DatabaseReference getNoteRoot() {
        return noteRoot;
    }

    public void setNoteRoot(DatabaseReference noteRoot) {
        this.noteRoot = noteRoot;
    }



    public StoreModel(){
        noteRoot = FirebaseDatabase.getInstance().getReference();
    }


    private DataSnapshot  dataRoot;
    public void getStoreList(final WhereInterface whereInterface, final Location vitrihientai, final int itemtieptheo, final int itemdaco){

        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                dataRoot = dataSnapshot;
                LayDanhSachQuanAn(dataSnapshot,whereInterface,vitrihientai,itemtieptheo,itemdaco);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };


        if(dataRoot != null){
            LayDanhSachQuanAn(dataRoot,whereInterface,vitrihientai,itemtieptheo,itemdaco);
        }else{
            noteRoot.addListenerForSingleValueEvent(valueEventListener);
        }

    }


    private void LayDanhSachQuanAn(DataSnapshot dataSnapshot, WhereInterface whereInterface,
                                   Location vitrihientai, int itemtieptheo, int itemdaco){

        DataSnapshot data= dataSnapshot.child("quanans");
        // Get list store
        int i = 0 ;

        for(DataSnapshot value : data.getChildren()){

            if( i == itemtieptheo){

                break;

            }
            if(i< itemdaco){
                i++;
                continue;

            }
            i++;
            StoreModel store = value.getValue(StoreModel.class);
            store.setMaquanan(value.getKey());

            // Get list image's store by id
            DataSnapshot picData = dataSnapshot.child("hinhanhquanans").child(value.getKey());

            List<String> picList = new ArrayList<>();

            for(DataSnapshot picValue : picData.getChildren()){
                picList.add(picValue.getValue(String.class));
            }

            store.setHinhanhquanan(picList);

            // Get comment
            DataSnapshot snapshotBinhLuan = dataSnapshot.child("binhluans").child(store.getMaquanan());
            List<BinhLuanModel> binhLuanModels = new ArrayList<>();
            for(DataSnapshot valueBinhLuan : snapshotBinhLuan.getChildren()){
                BinhLuanModel binhLuanModel = valueBinhLuan.getValue(BinhLuanModel.class);
                binhLuanModel.setMabinhluan(valueBinhLuan.getKey());
                UserModel thanhVienModel = dataSnapshot.child("thanhviens").child(binhLuanModel.getMauser()).getValue(UserModel.class);
                binhLuanModel.setThanhVienModel(thanhVienModel);

                List<String> hinhanhBinhLuanList = new ArrayList<>();
                DataSnapshot snapshotNodeHinhAnhBL = dataSnapshot.child("hinhanhbinhluans").child(binhLuanModel.getMabinhluan());
                for (DataSnapshot valueHinhBinhLuan : snapshotNodeHinhAnhBL.getChildren()){
                    hinhanhBinhLuanList.add(valueHinhBinhLuan.getValue(String.class));
                }
                binhLuanModel.setHinhanhList(hinhanhBinhLuanList);

                binhLuanModels.add(binhLuanModel);
            }

            store.setBinhluanModelList(binhLuanModels);


            // Chi nhanh quan an
            DataSnapshot snapshotChiNhanhQuanAn = dataSnapshot.child("chinhanhquanans").child(store.getMaquanan());
            List<ChiNhanhQuanAnModel> chiNhanhQuanAnModels = new ArrayList<>();

            for(DataSnapshot valueChiNhanhQuanAn : snapshotChiNhanhQuanAn.getChildren()){
                ChiNhanhQuanAnModel chiNhanhQuanAnModel = valueChiNhanhQuanAn.getValue(ChiNhanhQuanAnModel.class);
                Location vitriquanan = new Location("");
                vitriquanan.setLatitude(chiNhanhQuanAnModel.getLatitude());
                vitriquanan.setLongitude(chiNhanhQuanAnModel.getLongitude());

                double khoangcah=vitrihientai.distanceTo(vitriquanan)/1000;
                chiNhanhQuanAnModel.setKhoangcach(khoangcah);
                chiNhanhQuanAnModels.add(chiNhanhQuanAnModel);
                //Log.d("kiemtra",khoangcah + "-" + chiNhanhQuanAnModel.getDiachi());

            }


            store.setChiNhanhQuanAnModelList(chiNhanhQuanAnModels);

            // Enable WhereInterface in WhereController
            whereInterface.getStoreListModel(store);
        }

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (giaohang ? 1 : 0));
        dest.writeString(giodongcua);
        dest.writeString(giomocua);
        dest.writeString(tenquanan);
        dest.writeString(videogioithieu);
        dest.writeString(maquanan);
        dest.writeLong(giatoida);
        dest.writeLong(giatoithieu);
        dest.writeLong(luotthich);
        dest.writeStringList(tienich);
        dest.writeStringList(hinhanhquanan);
        //dest.writeTypedList(bitmapList);
        dest.writeTypedList(chiNhanhQuanAnModelList);
        dest.writeTypedList(binhluanModelList);
    }
}
